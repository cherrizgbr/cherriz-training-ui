package de.cherriz.training.ui.afterburner.presentation.main;

import de.cherriz.training.ui.afterburner.presentation.input.InputView;
import de.cherriz.training.ui.afterburner.presentation.list.ListView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class MainPresenter implements Initializable {

    @FXML
    private BorderPane main;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        main.setTop(new InputView().getView());
        main.setCenter(new ListView().getView());
    }

}