package de.cherriz.training.ui.afterburner.presentation.list;

import de.cherriz.training.ui.afterburner.service.notiz.NotizService;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class ListPresenter implements Initializable {

    @FXML
    public ListView<String> lstItems;

    @Inject
    private NotizService service;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lstItems.itemsProperty().bindBidirectional(service.getNoteListProperty());
    }

}