package de.cherriz.training.ui.afterburner.service.notiz;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;

public class NotizService {

    public ListProperty<String> itemList = new SimpleListProperty<>();

    public NotizService() {
        List<String> list = new ArrayList<>();
        itemList.setValue(FXCollections.observableArrayList(list));
    }

    public void addNote(String text) {
        itemList.add(text);
    }

    public ListProperty<String> getNoteListProperty() {
        return itemList;
    }

}