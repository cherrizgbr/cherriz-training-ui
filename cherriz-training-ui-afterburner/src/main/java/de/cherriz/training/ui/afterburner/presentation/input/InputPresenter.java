package de.cherriz.training.ui.afterburner.presentation.input;

import de.cherriz.training.ui.afterburner.service.notiz.NotizService;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class InputPresenter implements Initializable, EventHandler<ActionEvent> {

    @FXML
    public Button btnAdd;

    @FXML
    public TextField txtItem;

    @Inject
    private NotizService service;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObjectProperty<EventHandler<ActionEvent>> addProperty = new SimpleObjectProperty<>();
        addProperty.set(this);
        btnAdd.onActionProperty().bind(addProperty);
        txtItem.setText(resources.getString("default"));
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource().equals(btnAdd)) {
            service.addNote(txtItem.getText());
            txtItem.setText("");
        }
    }

}
