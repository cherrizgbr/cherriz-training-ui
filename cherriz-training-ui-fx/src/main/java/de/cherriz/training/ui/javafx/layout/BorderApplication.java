package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class BorderApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane top = new Pane();
        top.setStyle(cssDefault);
        top.setPrefHeight(100);

        Pane left = new Pane();
        left.setStyle(cssDefault);
        left.setPrefWidth(100);

        Pane center = new Pane();
        center.setStyle(cssDefault);

        Pane right = new Pane();
        right.setStyle(cssDefault);
        right.setPrefWidth(100);

        Pane bottom = new Pane();
        bottom.setStyle(cssDefault);
        bottom.setPrefHeight(100);

        BorderPane border = new BorderPane();
        border.setTop(top);
        border.setLeft(left);
        border.setCenter(center);
        border.setRight(right);
        border.setBottom(bottom);

        Scene scene = new Scene(border, 400, 400);

        primaryStage.setTitle("BorderPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}