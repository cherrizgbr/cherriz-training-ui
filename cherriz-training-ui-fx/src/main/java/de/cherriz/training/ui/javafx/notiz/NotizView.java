package de.cherriz.training.ui.javafx.notiz;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class NotizView {

    @FXML
    public Button btnAdd;

    @FXML
    public TextField txtItem;

    @FXML
    public ListView lstItems;

}