package de.cherriz.training.ui.javafx.notiz;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;

public class NotizModel {

    public StringProperty item = new SimpleStringProperty();

    public ListProperty<String> itemList = new SimpleListProperty<>();

    public NotizModel(){
        List<String> list = new ArrayList<>();
        itemList.setValue(FXCollections.observableArrayList(list));
    }

}