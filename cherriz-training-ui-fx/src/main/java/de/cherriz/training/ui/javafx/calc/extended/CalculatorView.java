package de.cherriz.training.ui.javafx.calc.extended;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class CalculatorView {

    @FXML
    public Button btn0;

    @FXML
    public Button btn1;

    @FXML
    public Button btn2;

    @FXML
    public Button btn3;

    @FXML
    public Button btn4;

    @FXML
    public Button btn5;

    @FXML
    public Button btn6;

    @FXML
    public Button btn7;

    @FXML
    public Button btn8;

    @FXML
    public Button btn9;

    @FXML
    public Button btnPlus;

    @FXML
    public Button btnMinus;

    @FXML
    public Button btnMultiply;

    @FXML
    public Button btnDivide;

    @FXML
    public Button btnCalculate;

    @FXML
    public Button btnClear;

    @FXML
    public Label lblDisplay;

}
