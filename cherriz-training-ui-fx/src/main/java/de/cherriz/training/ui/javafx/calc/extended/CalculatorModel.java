package de.cherriz.training.ui.javafx.calc.extended;

import javafx.beans.property.*;

public class CalculatorModel {

    private StringProperty consoleValue = new SimpleStringProperty();

    private IntegerProperty memoryValue = new SimpleIntegerProperty();

    private ObjectProperty<Operation> currentOperation = new SimpleObjectProperty<>();

    public CalculatorModel(){
        init();
    }

    public StringProperty consoleValueProperty() {
        return consoleValue;
    }

    public String getConsoleValue() {
        return consoleValue.get();
    }

    public void setConsoleValue(String consoleValue) {
        this.consoleValue.set(consoleValue);
    }

    public void setMemoryValue(int memoryValue) {
        this.memoryValue.set(memoryValue);
    }

    public int getMemoryValue() {
        return memoryValue.get();
    }

    public void setCurrentOperation(Operation currentOperation) {
        this.currentOperation.set(currentOperation);
    }

    public Operation getCurrentOperation() {
        return currentOperation.get();
    }

    public void init(){
        setConsoleValue("0");
        setMemoryValue(0);
        setCurrentOperation(Operation.None);
    }
}