package de.cherriz.training.ui.javafx.calc.extended;

public enum Operation {

    None, Plus, Minus, Multiply, Divide

}
