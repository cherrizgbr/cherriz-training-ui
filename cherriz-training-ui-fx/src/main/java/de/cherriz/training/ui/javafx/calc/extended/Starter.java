package de.cherriz.training.ui.javafx.calc.extended;

import javafx.application.Application;
import javafx.stage.Stage;

public class Starter extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        CalculatorController controller = new CalculatorController();
        controller.show();
    }

}