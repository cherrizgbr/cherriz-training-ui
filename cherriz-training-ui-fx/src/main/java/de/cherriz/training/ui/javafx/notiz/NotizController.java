package de.cherriz.training.ui.javafx.notiz;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class NotizController extends Stage implements EventHandler<ActionEvent> {

    private NotizView view;

    private NotizModel model;

    public NotizController() throws IOException {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/dialog/notiz.fxml"));
        setScene(new Scene(loader.<Parent>load()));
        setTitle("Notiz");

        view = loader.getController();
        model = new NotizModel();

        bindViewToModel();
        addListener();
    }

    private void bindViewToModel() {
        view.txtItem.textProperty().bindBidirectional(model.item);
        view.lstItems.itemsProperty().bindBidirectional(model.itemList);
    }

    private void addListener() {
        ObjectProperty<EventHandler<ActionEvent>> addProperty = new SimpleObjectProperty<>();
        addProperty.set(this);
        view.btnAdd.onActionProperty().bind(addProperty);
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource().equals(view.btnAdd)) {
            String item = model.item.getValue();
            model.item.setValue("");
            model.itemList.add(item);
        }
    }

}