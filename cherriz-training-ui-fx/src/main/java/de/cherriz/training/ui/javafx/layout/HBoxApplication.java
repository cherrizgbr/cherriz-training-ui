package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class HBoxApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn1 = new Pane();
        pn1.setStyle(cssDefault);
        pn1.setPrefWidth(100);

        Pane pn2 = new Pane();
        pn2.setStyle(cssDefault);
        pn2.setPrefWidth(100);

        Pane pn3 = new Pane();
        pn3.setStyle(cssDefault);
        pn3.setPrefWidth(100);

        HBox hBox = new HBox();
        hBox.setPadding(new Insets(10, 10, 10, 10));
        hBox.setSpacing(10);

        hBox.getChildren().addAll(pn1, pn2, pn3);

        Scene scene = new Scene(hBox, 400, 100);

        primaryStage.setTitle("HBox");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}