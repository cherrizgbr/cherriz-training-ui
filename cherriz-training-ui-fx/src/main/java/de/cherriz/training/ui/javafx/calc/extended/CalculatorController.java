package de.cherriz.training.ui.javafx.calc.extended;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class CalculatorController extends Stage implements EventHandler<ActionEvent> {

    private CalculatorView view;

    private CalculatorModel model;

    public CalculatorController() throws IOException {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/dialog/calculator.fxml"));
        setScene(new Scene(loader.<Parent>load()));
        setTitle("Calculator");

        view = loader.getController();
        model = new CalculatorModel();

        bindViewToModel();
        addListener();
    }

    private void bindViewToModel() {
        view.lblDisplay.textProperty().bind(model.consoleValueProperty());
    }

    private void addListener() {
        view.btn0.setOnAction(this);
        view.btn1.setOnAction(this);
        view.btn2.setOnAction(this);
        view.btn3.setOnAction(this);
        view.btn4.setOnAction(this);
        view.btn5.setOnAction(this);
        view.btn6.setOnAction(this);
        view.btn7.setOnAction(this);
        view.btn8.setOnAction(this);
        view.btn9.setOnAction(this);

        view.btnPlus.setOnAction(this);
        view.btnMinus.setOnAction(this);
        view.btnMultiply.setOnAction(this);
        view.btnDivide.setOnAction(this);
        view.btnCalculate.setOnAction(this);
        view.btnClear.setOnAction(this);
    }

    @Override
    public void handle(ActionEvent event) {
        Object source = event.getSource();

        if (view.btn0.equals(source)) {
            appendConsoleValue("0");
        } else if (view.btn1.equals(source)) {
            appendConsoleValue("1");
        } else if (view.btn2.equals(source)) {
            appendConsoleValue("2");
        } else if (view.btn3.equals(source)) {
            appendConsoleValue("3");
        } else if (view.btn4.equals(source)) {
            appendConsoleValue("4");
        } else if (view.btn5.equals(source)) {
            appendConsoleValue("5");
        } else if (view.btn6.equals(source)) {
            appendConsoleValue("6");
        } else if (view.btn7.equals(source)) {
            appendConsoleValue("7");
        } else if (view.btn8.equals(source)) {
            appendConsoleValue("8");
        } else if (view.btn9.equals(source)) {
            appendConsoleValue("9");
        } else if (view.btnPlus.equals(source)) {
            calculate(Operation.Plus);
        } else if (view.btnMinus.equals(source)) {
            calculate(Operation.Minus);
        } else if (view.btnMultiply.equals(source)) {
            calculate(Operation.Multiply);
        } else if (view.btnDivide.equals(source)) {
            calculate(Operation.Divide);
        } else if (view.btnCalculate.equals(source)) {
            calculate(Operation.None);
        } else if (view.btnClear.equals(source)) {
            model.init();
        }
    }

    private void appendConsoleValue(String value) {
        if (model.getConsoleValue().equals("0")) {
            model.setConsoleValue(value);
        } else {
            model.setConsoleValue(model.getConsoleValue() + value);
        }
    }

    private void calculate(Operation operation) {
        switch (model.getCurrentOperation()) {
            case Plus:
                applyOperation((a, b) -> a + b);
                break;
            case Minus:
                applyOperation((a, b) -> a - b);
                break;
            case Multiply:
                applyOperation((a, b) -> a * b);
                break;
            case Divide:
                applyOperation((a, b) -> a / b);
                break;
            case None:
                applyOperation((a, b) -> b);
                break;
        }

        model.setCurrentOperation(operation);

        if(operation == Operation.None){
            model.setConsoleValue(Integer.valueOf(model.getMemoryValue()).toString());
        }else{
            model.setConsoleValue("0");
        }
    }

    private void applyOperation(MathOperation operation) {
        int result = operation.perform(model.getMemoryValue(), Integer.parseInt(model.getConsoleValue()));

        model.setMemoryValue(result);
    }

    interface MathOperation {
        int perform(int a, int b);
    }

}