package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class VBoxApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn1 = new Pane();
        pn1.setStyle(cssDefault);
        pn1.setPrefHeight(100);

        Pane pn2 = new Pane();
        pn2.setStyle(cssDefault);
        pn2.setPrefHeight(100);

        Pane pn3 = new Pane();
        pn3.setStyle(cssDefault);
        pn3.setPrefHeight(100);

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setSpacing(10);

        vBox.getChildren().addAll(pn1, pn2, pn3);

        Scene scene = new Scene(vBox, 200, 400);

        primaryStage.setTitle("VBox");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}