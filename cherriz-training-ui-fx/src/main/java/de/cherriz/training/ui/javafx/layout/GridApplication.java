package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class GridApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn1 = new Pane();
        pn1.setStyle(cssDefault);
        pn1.setPrefSize(200,100);

        Pane pn2 = new Pane();
        pn2.setStyle(cssDefault);
        pn2.setPrefSize(100,200);

        Pane pn3 = new Pane();
        pn3.setStyle(cssDefault);
        pn3.setPrefSize(100,100);

        Pane pn4 = new Pane();
        pn4.setStyle(cssDefault);
        pn4.setPrefSize(100,100);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        Scene scene = new Scene(grid, 300, 200);
        grid.add(pn1, 0, 0, 2, 1);
        grid.add(pn2, 3, 0, 1, 2);
        grid.add(pn3, 0, 1);
        grid.add(pn4, 1, 1);

        primaryStage.setTitle("GridPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}