package de.cherriz.training.ui.javafx.calc.simple;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class SimpleCalc extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField txtValueA = new TextField();
        Label lblPlus = new Label("+");
        TextField txtValueB = new TextField();
        Label lblResult = new Label("=");

        HBox hBox = new HBox();
        hBox.setPadding(new Insets(10, 10, 10, 10));
        hBox.setSpacing(10);

        hBox.getChildren().addAll(txtValueA, lblPlus, txtValueB, lblResult);

        Scene scene = new Scene(hBox, 450, 50);

        primaryStage.setTitle("SimpleCalc");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}