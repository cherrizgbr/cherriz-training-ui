package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class FlowApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn1 = new Pane();
        pn1.setStyle(cssDefault);
        pn1.setPrefSize(100, 100);

        Pane pn2 = new Pane();
        pn2.setStyle(cssDefault);
        pn2.setPrefSize(100, 100);

        Pane pn3 = new Pane();
        pn3.setStyle(cssDefault);
        pn3.setPrefSize(100, 100);

        Pane pn4 = new Pane();
        pn4.setStyle(cssDefault);
        pn4.setPrefSize(100, 100);

        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(4);
        flow.setHgap(4);

        flow.getChildren().addAll(pn1, pn2, pn3, pn4);

        Scene scene = new Scene(flow, 350, 250);

        primaryStage.setTitle("FlowPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}