package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AnchorApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn = new Pane();
        pn.setStyle(cssDefault);
        pn.setPrefSize(100, 100);

        AnchorPane anchor = new AnchorPane();

        anchor.getChildren().add(pn);
        AnchorPane.setTopAnchor(pn, 10.0);
        AnchorPane.setLeftAnchor(pn, 10.0);

        Scene scene = new Scene(anchor, 300, 300);

        primaryStage.setTitle("AnchorPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}