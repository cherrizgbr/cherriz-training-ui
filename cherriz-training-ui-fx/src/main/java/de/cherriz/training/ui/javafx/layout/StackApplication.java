package de.cherriz.training.ui.javafx.layout;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class StackApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        String cssDefault = "-fx-border-color: grey;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: solid;\n";

        Pane pn = new Pane();
        pn.setStyle(cssDefault);

        Button btn = new Button("Test");

        StackPane stack = new StackPane();

        stack.getChildren().addAll(pn, btn);

        Scene scene = new Scene(stack, 300, 100);

        primaryStage.setTitle("StackPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}