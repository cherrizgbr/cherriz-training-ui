package de.cherriz.training.ui.javafx.calc.simple;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

public class SimpleCalcAdvanced extends Application {

    private IntegerProperty valA = new SimpleIntegerProperty();

    private IntegerProperty valB = new SimpleIntegerProperty();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField txtValueA = new TextField();
        Label lblPlus = new Label("+");
        TextField txtValueB = new TextField();
        Label lblResult = new Label("=");

        HBox hBox = new HBox();
        hBox.setPadding(new Insets(10, 10, 10, 10));
        hBox.setSpacing(10);

        hBox.getChildren().addAll(txtValueA, lblPlus, txtValueB, lblResult);

        StringConverter<Number> converter = new NumberStringConverter();
        Bindings.bindBidirectional(txtValueA.textProperty(), valA, converter);
        Bindings.bindBidirectional(txtValueB.textProperty(), valB, converter);

        lblResult.textProperty().bind(Bindings.concat("= ", Bindings.add(valA, valB)));

        Scene scene = new Scene(hBox, 450, 50);

        primaryStage.setTitle("SimpleCalcAdvanced");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}