package de.cherriz.training.ui.swing.image;

public class Starter {

    public static void main(String args[]) {
        String[] images = {"/images/bild1.jpg", "/images/bild2.jpg", "/images/bild3.jpg"};
        ImageViewer viewer = new ImageViewer(images);
    }

}