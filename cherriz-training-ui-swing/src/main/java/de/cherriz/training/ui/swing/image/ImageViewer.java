package de.cherriz.training.ui.swing.image;

import javax.swing.*;
import java.awt.*;

public class ImageViewer {

    private String[] images;

    private int imgPointer = 0;

    private JButton prev;

    private JButton next;

    private JLabel imgLabel;

    public ImageViewer(String url[]) {
        images = url;

        JFrame window = createFrame();
        appendControls(window);
        addListener();

        showImage();
        window.setVisible(true);
    }

    private JFrame createFrame() {
        JFrame window = new JFrame();
        window.setLayout(new BorderLayout());

        window.setSize(850, 600);
        window.setTitle("Image Viewer");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window.setVisible(true);

        return window;
    }

    private void appendControls(JFrame frame) {
        imgLabel = new JLabel();
        Font buttonFont = new Font("Arial", Font.BOLD, 30);
        prev = new JButton("<<");
        prev.setFont(buttonFont);
        next = new JButton(">>");
        next.setFont(buttonFont);

        frame.setLayout(new BorderLayout());
        frame.add(imgLabel, BorderLayout.CENTER);
        frame.add(prev, BorderLayout.WEST);
        frame.add(next, BorderLayout.EAST);
    }

    private void addListener() {
        prev.addActionListener(e -> prevImage());
        next.addActionListener(e -> nextImage());
    }

    public void nextImage() {
        imgPointer++;
        if (imgPointer >= images.length) {
            imgPointer = 0;
        }
        showImage();
    }

    public void prevImage() {
        imgPointer--;
        if (imgPointer < 0) {
            imgPointer = images.length - 1;
        }
        showImage();
    }

    private void showImage() {
        ImageIcon imageIcon = new ImageIcon(this.getClass().getResource(images[imgPointer]));
        imgLabel.setIcon(imageIcon);
    }

}