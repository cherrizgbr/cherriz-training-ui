package de.cherriz.training.ui.awt.list;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SimpleList extends Frame {

    private Choice cMenge;

    private TextField item;

    private Button add;

    private List items;

    public SimpleList() {
        setTitle("Einkaufsliste");
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        Panel pnl = createControlPanel();
        add(pnl, BorderLayout.NORTH);
        items = new List();
        add(items, BorderLayout.CENTER);

        addListener();

        setSize(300, 200);
        setVisible(true);
    }

    private Panel createControlPanel() {
        Label lblMenge = new Label("Menge: ");

        cMenge = new Choice();
        cMenge.add("1");
        cMenge.add("2");
        cMenge.add("3");

        item = new TextField();

        add = new Button("Hinzufügen");

        Panel ctrPanel = new Panel();
        ctrPanel.setLayout(new GridLayout(1, 4, 5, 5));
        ctrPanel.setSize(300, 25);
        ctrPanel.add(lblMenge);
        ctrPanel.add(cMenge);
        ctrPanel.add(item);
        ctrPanel.add(add);

        return ctrPanel;
    }

    private void addListener() {
        //Neue Elemente in Liste aufnehmen
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                items.add(cMenge.getSelectedItem() + " x " + item.getText());
                item.setText("");
                cMenge.select(0);
            }
        });

        // Anwendung beenden, wenn Fenster geschlossen wird.
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                e.getWindow().dispose();
                System.exit(0);
            }
        });
    }

}